// CUSTOMER-CLASS
class Customer {
	constructor(email){
		if (typeof email === 'string') {
			this.email == email;
		} else {
			this.email == undefined;
		}
		this.cart = new Cart()
		this.orders = []
	}

	checkOut(){
		if (this.cart.contents.length === 0) {
			console.log("Your Cart is empty! Nothing not checkout.")
			return this
		} else {
			this.orders.push({
				products: this.cart.contents,
				totalAmount: this.cart.computeTotal().totalAmount
			})
			console.log("Thank you for your purchase!")
		}
		return this
	}
}


// PRODUCT-CLASS
class Product {
	constructor(name, price, isActive){
		if (typeof name === 'string') {
			this.name = name;
		} else {
			this.name = undefined;
		}

		if (typeof price === 'number') {
			this.price = price;
		} else {
			this.price = undefined;
		}

		this.isActive = true;
	}

	archive(){
		if (this.isActive) {
			this.isActive = false
			console.log(`${this.name} Active status is updated to ${this.isActive}.`)
		} else {
			console.log(`Unnecessary action! Status of this product is already ${this.isActive}.`)
		}
	}

	updatePrice(price){
		if (typeof this.price === 'number' && price >= 0) {
			this.price = price
			console.log(`The updated price for ${this.name} is now : ${this.price}`)
		} else {
			console.log('Invalid input for price!')
		}
		return this
	}
}


// CART-CLASS 
class Cart {
	constructor(contents, totalAmount){
		this.contents = []
		this.totalAmount = 0
	}

	addToCart(product, productQuantity){
		if (typeof product === 'object' && product.isActive === true){
			if (typeof productQuantity === 'number' && productQuantity > 0) {
				this.contents.push({product, quantity: productQuantity})
				console.log(`${product.name} is successfully added to your cart Qty:${productQuantity}`)
				return this;

			} else {
				console.log('Invalid input for quantity!')
				return this
			}
		} else {
			console.log(`Product: ${product} Not Found!`)
		}
		
	}

	showCartContents(){
		if (this.contents.length === 0){
			console.log("Your cart is empty!")
			return this
		} else {
			console.log(this.contents)
			return this
		}
	}

	updateProductQuantity(productName, newQuantity){
		let existInCart = false
		if (typeof productName === 'string' && newQuantity > 0) {
			this.contents.forEach(content => {
				if (content.product.name === productName){
					content.quantity = newQuantity
					existInCart = true
				}
			})
		} else {
			console.log("Invalid Input!")
			return this
		}

		if (existInCart == true) {
			console.log(`Product: ${productName} new Qty: ${newQuantity}`)
			return this
		} else {
			console.log(`This product doesn't exist in your cart!`)
			return this
		}
	}

	clearCartContents(){
		if(this.contents.length === 0) {
			console.log('Your cart is empty!')
			return this
		} else {
			this.contents = []
			this.totalAmount = 0
			console.log('Your cart now is empty.')
			return this
		}
	}

	computeTotal(){
		let total = 0
		if (this.contents.length === 0) {
			console.log('Your cart is empty! Your totalAmount: 0')
			return this
		} else {
			this.contents.forEach(content => {
				total += content.product.price * content.quantity;
			})
		}
		this.totalAmount = total
		return this
	} 
}


// test statements / for testing
const john = new Customer('john@mail.com')

const prodA = new Product('soap', 9.99)
const prodB = new Product('shampoo', 12.99)
const prodC = new Product ('toothbrush', 4.99)
const prodD = new Product ('toothpaste', 14.99)

john.cart.addToCart(prodA, 3)
john.cart.addToCart(prodB, 2)

// john.cart.computeTotal()
john.cart.updateProductQuantity('soap', 5)
john.cart.showCartContents()
// john.cart.clearCartContents()
// john.cart.showCartContents()
john.checkOut()